const express = require('express')
const mysql = require('mysql')

const app = express()
const port = 3000

const dbConfig = {
    host: 'db',
    user: 'root',
    password: 'root',
    database: 'nodedb'
}

const connection = mysql.createConnection(dbConfig)

connection.connect(err => {
    if (err) {
      console.error('Error connecting to database:', err);
      return;
    }
    console.log('Connected to MySQL database.');
})

app.get('/', (req, res) => {
    const name = 'John Doe';
    const insertQuery = `INSERT INTO people (name) VALUES ('${name}')`;

    connection.query(insertQuery, err => {
        if (err) {
            console.error('Error inserting data into database:', err);
            res.status(500).send('Error inserting data into database.');
            return;
        }

        console.log('Data inserted into database.');

        const selectQuery = 'SELECT * FROM people';

        connection.query(selectQuery, (err, results) => {
            if (err) {
                console.error('Error retrieving data from database:', err);
                res.status(500).send('Error retrieving data from database.');
                return;
            }

            console.log('Data retrieved from database:', results);

            const namesList = results.map(result => result.name).join(', ');

            res.send(`
                <h1>Full Cycle Rocks!</h1>
                <p>Data inserted into database.</p>
                <p>Names in database: <b> ${namesList} </b></p>
            `);
        });
    });
});

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});
